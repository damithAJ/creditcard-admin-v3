import React from "react";

const TOKEN_KEY = '@@8379171629';
const jwt = require('jsonwebtoken');

export const login = (token) => {
    localStorage.setItem(TOKEN_KEY, token);
};


export const getToken = () => {
    const token = localStorage.getItem(TOKEN_KEY);
    return token;
};

export const userLogOut = () => {
    console.log('remove token....');
    localStorage.removeItem(TOKEN_KEY);
};

export const isLogin = () => {
    const token = getToken();
    if (token) {
        let decoded = jwt.decode(token);
        console.log('======= decoded token =========');
        console.log(decoded);
        if (decoded && decoded.isLogin) {
            return true;
        }
        console.log('User cant be access main console : logout');
        return false;
    }
};

export const userDetails = () => {
    const token = getToken();
    if (token) {
        console.log(jwt.decode(token));
        return jwt.decode(token);
    }
};