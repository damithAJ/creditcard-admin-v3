import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './app.css';
import 'antd/dist/antd.css';
import '../src/assets/icon/feather/iconfont.css';
import '../src/assets/icon/typicons/typicons.css';


import {
    BrowserRouter as Router,
    Link,
    Switch,
    Route // for later
} from 'react-router-dom';

import SingIn from '../src/component/sign-in/signIn';
import Console from '../src/component/console/console';

import NoMatch404 from '../src/component/error/404';


function App() {
    return (
        <>
            <Router>
                <Switch>
                    <Route exact path='/' component={SingIn}/>
                    {/*<Route path="*" component={NoMatch404}/>*/}

                    <Route path='/console' component={Console}/>
                    <Route path='/console:id' component={Console}/>
                    {/*<Route path={`/topics/:topicId`} component={Topic}/>*/}
                </Switch>

            </Router>
        </>
    );
}

// const { match } = this.props // coming from React Router.
// console.log(match.path) // /topics/:topicId/:subId
// console.log(match.url) // /topics/react-router/url-parameters

export default App;
