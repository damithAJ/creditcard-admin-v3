import {
    CATEGORY_REQUEST_PENDING,
    CATEGORY_REQUEST_SUCCESS,
    CATEGORY_CREATE_SUCCESS,
    CATEGORY_CREATE_FAILURE,
} from '../constants/types';

const initialState = {
    result: [],
    error: '',
    loading: false,
};

export default (state = initialState, action) => {
    switch (action.type) {
        case CATEGORY_REQUEST_PENDING:
            return {
                ...state,
                error: '',
                result: [],
                loading: action.loading,
            };
        case CATEGORY_REQUEST_SUCCESS:
            return {
                ...state,
                error: '',
                loading: action.loading,
                result: action.payload,
            };
        case CATEGORY_CREATE_SUCCESS:
            return {
                ...state,
                error: '',
                loading: action.loading,
                result: action.payload,
            };
        case CATEGORY_CREATE_FAILURE:
            return {
                ...state,
                error: '',
                loading: action.loading,
                result: action.payload,
            };
        default:
            return state
    }
};
