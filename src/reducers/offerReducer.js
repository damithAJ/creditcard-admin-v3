import {
    OFFER_REQUEST_PENDING,
    OFFER_REQUEST_SUCCESS,
    OFFER_REQUEST_FAILURE,
} from '../constants/types';

const initialState = {
    result: [],
    error: '',
    loading: false,
};

export default (state = initialState, action) => {
    switch (action.type) {
        case OFFER_REQUEST_SUCCESS:
            return {
                ...state,
                error: '',
                result: action.payload,
                loading: action.loading,
            };
        case OFFER_REQUEST_PENDING:
            return {
                ...state,
                error: '',
                loading: action.loading,
                result: action.payload,
            };
        case OFFER_REQUEST_FAILURE:
            return {
                ...state,
                error: '',
                loading: action.loading,
                result: action.payload,
            };
        default:
            return state
    }
};
