import {
    CLIENT_REQUEST_PENDING,
    CLIENT_REQUEST_SUCCESS,
    CLIENT_REQUEST_FAILURE,
} from '../constants/types';

const initialState = {
    result: [],
    error: '',
    loading: false,
};

export default (state = initialState, action) => {
    switch (action.type) {
        case CLIENT_REQUEST_SUCCESS:
            return {
                ...state,
                error: '',
                result: action.payload,
                loading: action.loading,
            };
        case CLIENT_REQUEST_PENDING:
            return {
                ...state,
                error: '',
                loading: action.loading,
                result: action.payload,
            };
        case CLIENT_REQUEST_FAILURE:
            return {
                ...state,
                error: '',
                loading: action.loading,
                result: action.payload,
            };
        default:
            return state
    }
};
