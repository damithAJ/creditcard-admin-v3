import {
    BANK_REQUEST_PENDING,
    BANK_REQUEST_SUCCESS,
    BANK_REQUEST_FAILURE,
} from '../constants/types';

const initialState = {
    result: [],
    error: '',
    loading: false,
};

export default (state = initialState, action) => {
    switch (action.type) {
        case BANK_REQUEST_SUCCESS:
            return {
                ...state,
                error: '',
                result: action.payload,
                loading: action.loading,
            };
        case BANK_REQUEST_PENDING:
            return {
                ...state,
                error: '',
                loading: action.loading,
                result: action.payload,
            };
        case BANK_REQUEST_FAILURE:
            return {
                ...state,
                error: '',
                loading: action.loading,
                result: action.payload,
            };
        default:
            return state
    }
};
