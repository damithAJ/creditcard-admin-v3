import categoryReducer from "./categoryReducer";
import bankReducer from "./bankReducer";
import clientReducer from "./clientReducer";
import offerReducer from "./offerReducer";
import {combineReducers} from "redux";

const allReducers = combineReducers({
    category: categoryReducer,
    bankData: bankReducer,
    clientData: clientReducer,
    offerData: offerReducer
});

export default allReducers;