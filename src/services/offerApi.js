import axios from 'axios';
import {
    OFFER_REQUEST_PENDING,
    OFFER_REQUEST_SUCCESS,
    OFFER_REQUEST_FAILURE
} from '../constants/types';
import {BASE_URL} from "../config/serverIP";
import {getToken} from '../utils';

const token = getToken();
let OFFER_URL = BASE_URL + 'offer/';


//================= get all category =============
export const getAllOffer = (body) => dispatch => {
    dispatch({
        type: OFFER_REQUEST_PENDING,
        payload: [],
        loading: true,
    });
    axios.get(OFFER_URL + 'get-all/', {data: body}, {
        headers: {
            'Content-Type': 'application/json',
            authorization: `Bearer ${token}`
        }
    }).then(res => {
        if (res && res.data && res.data.data && res.data.data.result) {
            dispatch({
                type: OFFER_REQUEST_SUCCESS,
                payload: res.data.data.result,
                loading: false
            });
        }
    }).catch(err => {
        dispatch({
            type: OFFER_REQUEST_FAILURE,
            payload: err,
            loading: false
        });
    });
};


//================= save offer =============
export const saveOffer = (body, callback) => {
    axios.post(OFFER_URL + 'create-new',
        {data: body}, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': token
            }
        })
        .then(res => {
            if (res && res.data && res.data.data && res.data.data.result) {
                callback(true);
            }
        })
        .catch(err => {
            callback(true);
            console.log('category save error');
            console.log(err);
        });
};


//================= save offer =============
export const acceptOffer = (body, callback) => {
    axios.post(OFFER_URL + 'accept-offer',
        {data: body}, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': token
            }
        })
        .then(res => {
            if (res && res.data && res.data.data && res.data.data.result) {
                callback(true);
            }
        })
        .catch(err => {
            callback(true);
            console.log('offer accept error...');
            console.log(err);
        });
};

//================= offer get by page id =============
export const getOfferByPageId = (pageNo, pageSize = {}) => {
    return new Promise(function (resolve, reject) {
        // res && res.data && res.data.data && res.data.data
        fetch(OFFER_URL + 'get-offer-by-pagination/' + pageNo + '/' + pageSize, {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        }).then(res => {
            return res.json();

        }).then(data => {
            if (data) {
                resolve(data);
            }
            else {
                //break operation and return empty list
                resolve(null);
            }
        }).catch(ex => {
            reject(ex);

        });
    });
};






