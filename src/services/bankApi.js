import axios from 'axios';
import {
    BANK_REQUEST_FAILURE,
    BANK_REQUEST_PENDING,
    BANK_REQUEST_SUCCESS
} from '../constants/types';
import {BASE_URL} from "../config/serverIP";
import {getToken} from '../utils';

const token = getToken();
let config = {
    headers: {
        'Content-Type': 'application/json',
        'Authorization': token
    }
};

//================= get all category =============
export const getAllBank = () => dispatch => {
    dispatch({
        type: BANK_REQUEST_PENDING,
        payload: [],
        loading: true,
    });
    axios.get(BASE_URL + 'bank/get-all', config)
        .then(res => {
            if (res && res.data && res.data.data && res.data.data.result) {
                dispatch({
                    type: BANK_REQUEST_SUCCESS,
                    payload: res.data.data.result,
                    loading: false
                });
            }
        })
        .catch(err => {
            dispatch({
                type: BANK_REQUEST_FAILURE,
                payload: err,
                loading: false
            });
        });
};


//================= save category =============
export const saveBank = (body, callback) => {
    axios.post(BASE_URL + 'bank/create-new', {data: body}, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': token
        }
    })
        .then(res => {
            if (res && res.data && res.data.data && res.data.data.result) {
                callback(true);
            }
        })
        .catch(err => {
            callback(false);
            console.log('bank save error');
            console.log(err);
        });
};







