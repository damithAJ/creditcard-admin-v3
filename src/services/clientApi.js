import axios from 'axios';
import {
    CLIENT_REQUEST_PENDING,
    CLIENT_REQUEST_SUCCESS,
    CLIENT_REQUEST_FAILURE,
} from '../constants/types';
import {BASE_URL} from "../config/serverIP";
import {getToken} from '../utils';

const token = getToken();
let CATEGORY_URL = BASE_URL + 'client/';


//================= get all category =============
export const getAllClient = (body) => dispatch => {
    dispatch({
        type: CLIENT_REQUEST_PENDING,
        payload: [],
        loading: true,
    });
    axios.get(CATEGORY_URL + 'get-all/', {data: body}, {
        headers: {
            'Content-Type': 'application/json',
            authorization: `Bearer ${token}`
        }
    }).then(res => {
        if (res && res.data && res.data.data && res.data.data.result) {
            dispatch({
                type: CLIENT_REQUEST_SUCCESS,
                payload: res.data.data.result,
                loading: false
            });
        }
    }).catch(err => {
        dispatch({
            type: CLIENT_REQUEST_FAILURE,
            payload: err,
            loading: false
        });
    });
};


//================= save category =============
export const saveClient = (body, callback) => {
    axios.post(CATEGORY_URL + 'create-new',
        {data: body}, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': token
            }
        })
        .then(res => {
            if (res && res.data && res.data.data && res.data.data.result) {
                callback(true);
            }
        })
        .catch(err => {
            callback(true);
            console.log('client save error');
            console.log(err);
        });
};







