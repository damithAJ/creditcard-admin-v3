import axios from 'axios';
import {getToken} from '../utils';
import {BASE_URL} from "../config/serverIP";

//====
export const signIn = (user, callback) => {
    axios.post(BASE_URL + 'admin/sign-in', user,
        {
            headers: {
                'Content-Type': 'application/json',
            }
        })
        .then(res => {
            if (res && res.data) {
                const dataStr = JSON.stringify(res.data);
                let token = JSON.parse(dataStr).data.token;
                localStorage.setItem('@@8379171629', token);
                callback(true, res.data);
            } else {
                callback(false, res.data);
            }
        })
        .catch(err => {
            callback(false, err);
        });
};