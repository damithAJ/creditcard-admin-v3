import axios from 'axios';
import {
    CATEGORY_REQUEST_FAILURE,
    CATEGORY_REQUEST_PENDING,
    CATEGORY_REQUEST_SUCCESS
} from '../constants/types';
import {BASE_URL} from "../config/serverIP";
import {getToken} from '../utils';

const token = getToken();
let CATEGORY_URL = BASE_URL + 'category/';


//================= get all category =============
export const getAllCategory = (body) => dispatch => {
    dispatch({
        type: CATEGORY_REQUEST_PENDING,
        payload: [],
        loading: true,
    });
    axios.get(CATEGORY_URL + 'get-all/', {data: body}, {
        headers: {
            'Content-Type': 'application/json',
            authorization: `Bearer ${token}`
        }
    }).then(res => {
        if (res && res.data && res.data.data && res.data.data.result) {
            dispatch({
                type: CATEGORY_REQUEST_SUCCESS,
                payload: res.data.data.result,
                loading: false
            });
        }
    }).catch(err => {
        dispatch({
            type: CATEGORY_REQUEST_FAILURE,
            payload: err,
            loading: false
        });
    });
};


//================= save category =============
export const saveCategory = (body, callback) => {
    axios.post(CATEGORY_URL + 'create-new',
        {data: body}, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': token
            }
        })
        .then(res => {
            console.log(res.data.data);
            if (res && res.data && res.data.data && res.data.data.result) {
                callback(true);
            }
        })
        .catch(err => {
            callback(true);
            console.log('category save error');
            console.log(err);
        });
};







