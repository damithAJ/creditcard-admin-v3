import React, {Component, useEffect, useState} from 'react';
import * as authentication from "../../services/authentication";
import {message, Menu, Dropdown} from 'antd';
import CreateNewCategory from "../category/createNewCategory";
import CreateNewBank from "../bank/createNewBank";
import CreateNewClient from "../client/createNewClient";
import CreateNewOffer from "../offer/createNewOffer";
import {Scrollbars} from 'react-custom-scrollbars';
import Category from "../category/category";
import Bank from "../bank/bank";
import Client from "../client/client";
import Offer from "../offer/offers";
import Admin from "../admin/admin";
import {connect} from "react-redux";
import {getAllCategory} from "../../services/categoryApi";
import {getAllBank} from "../../services/bankApi";
import {getAllClient} from "../../services/clientApi";
import {getAllOffer} from "../../services/offerApi";

import {
    Route,
    NavLink,
    Link,
    useRouteMatch
} from "react-router-dom";

class console extends Component {
    constructor(props) {
        super(props);
        this.state = {
            viewPointWidth: 0,
            viewPointHeight: 0,
            categoryName: null,
            openModalName: null
        }
        ;
        message.config({
            top: 10,
            maxCount: 3,
        });

        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
        this.openModalPanel = this.openModalPanel.bind(this);
    }


    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleOnScroll);
        window.removeEventListener('resize', this.updateWindowDimensions);
        window.addEventListener('scroll', this.listenToScroll);
    }

    updateWindowDimensions() {
        this.setState({viewPointWidth: window.innerWidth, viewPointHeight: window.innerHeight});
    }

    handleOnScroll = () => {
        // http://stackoverflow.com/questions/9439725/javascript-how-to-detect-if-browser-window-is-scrolled-to-bottom
        var scrollTop = (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop;
        var scrollHeight = (document.documentElement && document.documentElement.scrollHeight) || document.body.scrollHeight;
        var clientHeight = document.documentElement.clientHeight || window.innerHeight;
        // var scrolledToBottom = Math.ceil(scrollTop + clientHeight) >= scrollHeight;

        // if (scrolledToBottom) {
        //     this.setState(
        //         prevState => ({
        //             pageNo: prevState.pageNo + 1
        //         }),
        //         this.getOfferDataByPage
        //     );
        // }
    };

    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
        window.addEventListener('scroll', this.listenToScroll);
        window.addEventListener('scroll', this.handleOnScroll);
    }

    updateWindowDimensions() {
        this.setState({viewPointWidth: window.innerWidth, viewPointHeight: window.innerHeight});
    }

    isChangeLoadEvent(_status) {

    }

    //====== HandleInputChange ====
    handleInputChange(e) {
    }

    //goto child page
    gotToPage(_pageName) {
        switch (_pageName) {
            case 'admin':
                this.props.history.push("/console/admin");
                break;
            case 'category':
                this.props.history.push("/console/category");
                break;
            case 'bank':
                this.props.history.push("/console/bank");
                break;
            case 'client':
                this.props.history.push("/console/client");
                break;
            case 'offers':
                this.props.history.push("/console/offers");
                break;
            default:
                this.props.history.push("/");
                break;
        }
    }

    //create new item
    openModalPanel(_action) {
        switch (_action) {
            case 'newCategory':
                //create new category
                this.setState({
                    openModalName: _action
                });
                break;
            case 'newBank':
                //create new bank
                this.setState({
                    openModalName: _action
                });
                break;
            case 'newClient':
                //create new client
                this.setState({
                    openModalName: _action
                });
                break;
            case 'newOffer':
                //create new client
                //this.props.getAllBank();
                //this.props.getAllClient();
                this.props.getAllCategory();
                this.setState({
                    openModalName: _action
                });
                break;
            case 'closeMe':
                this.setState({
                    openModalName: null
                });
                break;
            default:
                break;
        }
    }


    render() {
        const {openModalName, categoryName, viewPointHeight} = this.state;
        const {pathname} = this.props.location;
        const menu = (
            <Menu>
                <Menu.Item>
                    <a target="_blank" rel="noopener noreferrer"
                       onClick={(e) => this.openModalPanel('newCategory')}>
                        Creat New category
                    </a>
                </Menu.Item>
                <Menu.Item>
                    <a target="_blank" rel="noopener noreferrer"
                       onClick={(e) => this.openModalPanel('newBank')}>
                        Creat New Bank
                    </a>
                </Menu.Item>
                <Menu.Item>
                    <a target="_blank" rel="noopener noreferrer"
                       onClick={(e) => this.openModalPanel('newClient')}>
                        Creat New Client
                    </a>
                </Menu.Item>
                <Menu.Item>
                    <a target="_blank" rel="noopener noreferrer"
                       onClick={(e) => this.openModalPanel('newOffer')}>
                        Creat New Offer
                    </a>
                </Menu.Item>
            </Menu>
        );
        return (

            <div>

                {/*======= create new category ========*/}
                {openModalName === 'newCategory' &&
                <CreateNewCategory
                    categoryName={this.categoryName}
                    getAllCategory={this.props.getAllCategory}
                    openModalPanel={this.openModalPanel}/>}
                {openModalName === 'newBank' &&
                <CreateNewBank
                    viewPointHeight={viewPointHeight}
                    getAllBank={this.props.getAllBank}
                    openModalPanel={this.openModalPanel}/>}
                {openModalName === 'newClient' &&
                <CreateNewClient
                    viewPointHeight={viewPointHeight}
                    getAllClient={this.props.getAllClient}
                    openModalPanel={this.openModalPanel}/>}
                {openModalName === 'newOffer' &&
                <CreateNewOffer
                    viewPointHeight={viewPointHeight}
                    getAllClient={this.props.getAllClient}
                    getAllCategory={this.props.getAllCategory}
                    isCategoryLoading={this.props.isCategoryLoading}
                    getAllBank={this.props.getAllBank}
                    categoryData={this.props.categoryData}
                    bankData={this.props.bankData}
                    isBankLoading={this.props.isBankLoading}
                    clientData={this.props.clientData}
                    isClientLoading={this.props.isClientLoading}
                    openModalName={openModalName}
                    openModalPanel={this.openModalPanel}/>}


                {/*===== console main wrapper =======*/}
                <section className="console-main-wrapper">
                    {/*======= Left menu ==========*/}
                    <section className="left-wrapper">
                        {/*logo-wrapper*/}
                        <div className="logo-wrapper">
                            <div className="logo-header d-flex">
                                creditcardoffers.lk
                                {/*<div className="tag-admin">admin</div>*/}
                            </div>
                        </div>
                        {/*menu-wrapper*/}
                        <div className="menu-wrapper">
                            {/*admin*/}
                            <NavLink activeClassName="active"
                                     to="/console/admin">
                                <div className="left-menu-item-wrp default d-flex">
                                    <div className="icon d-flex align-items-center">
                                        <i className="typcn typcn-user"/>
                                    </div>
                                    <div className="text text-capitalize d-flex align-items-center">
                                        admin
                                    </div>
                                </div>
                            </NavLink>
                            {/*Category*/}
                            <NavLink activeClassName="active"
                                     to="/console/category">
                                <div className="left-menu-item-wrp default d-flex">
                                    <div className="icon d-flex align-items-center">
                                        <i className="typcn typcn-feather"/>
                                    </div>
                                    <div className="text text-capitalize d-flex align-items-center">
                                        category
                                    </div>
                                </div>
                            </NavLink>
                            {/*bank*/}
                            <NavLink activeClassName="active"
                                     to="/console/bank">
                                <div className="left-menu-item-wrp defulat  d-flex ">
                                    <div className="icon d-flex align-items-center">
                                        <i className="typcn typcn-adjust-contrast"/>
                                    </div>
                                    <div className="text text-capitalize d-flex align-items-center">
                                        bank
                                    </div>
                                </div>
                            </NavLink>
                            {/*client*/}
                            <NavLink activeClassName="active"
                                     to="/console/client">
                                <div className="left-menu-item-wrp default d-flex">
                                    <div className="icon d-flex align-items-center">
                                        <i className="typcn typcn-group"/>
                                    </div>
                                    <div className="text text-capitalize d-flex align-items-center">
                                        client
                                    </div>
                                </div>
                            </NavLink>
                            {/*offer*/}
                            <NavLink activeClassName="active"
                                     to="/console/offers">
                                <div className="left-menu-item-wrp default d-flex ">
                                    <div className="icon d-flex align-items-center">
                                        <i className="typcn typcn-tags"/>
                                    </div>
                                    <div className="text text-capitalize d-flex align-items-center">
                                        offers
                                    </div>
                                </div>
                            </NavLink>
                        </div>
                    </section>

                    {/*======= console body ==========*/}
                    <section className="console-body-wrapper"
                             style={{height: this.state.viewPointHeight}}>
                        <div className="col-12">
                            <div className="body-header-wrapper d-flex align-items-center justify-content-between">

                                <div className="left-wrp">
                                    {pathname === '/console/admin' &&
                                    <h2>
                                        Admin
                                    </h2>
                                    }
                                    {pathname === '/console/category' &&
                                    <h2>
                                        Offer Category
                                    </h2>
                                    }
                                    {pathname === '/console/bank' &&
                                    <h2>
                                        Bank Details
                                    </h2>
                                    }
                                    {pathname === '/console/client' &&
                                    <h2>
                                        Client Details
                                    </h2>
                                    }
                                    {pathname === '/console/offers' &&
                                    <h2>
                                        Offer Details
                                    </h2>
                                    }
                                </div>

                                <div className="right-wrp">
                                    <div className="menu-item  d-flex">
                                        <Dropdown
                                            overlay={menu}>
                                            <div className="item fixed-bottom-right">
                                                <i className="typcn typcn-plus d-flex align-items-center"/>
                                            </div>
                                        </Dropdown>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div className="col-12">
                            <div className="body-content-wrapper"
                                 style={{height: this.state.viewPointHeight - 110}}>
                                <switch>
                                    {/*admin view */}
                                    <Route path={'/console/admin'}>
                                        <Admin viewPointHeight={this.viewPointHeight}/>
                                    </Route>
                                    <Scrollbars style={{height: this.state.viewPointHeight - 120}}>
                                        {/*category view */}
                                        <Route path={'/console/category'}>
                                            <Category viewPointHeight={this.viewPointHeight}/>
                                        </Route>
                                        {/*bank view */}
                                        <Route path={'/console/bank'}>
                                            <Bank viewPointHeight={this.viewPointHeight}/>
                                        </Route>
                                        {/*client view */}
                                        <Route path={'/console/client'}>
                                            <Client viewPointHeight={this.viewPointHeight}/>
                                        </Route>
                                        {/*offer view */}
                                        <Route path={'/console/offers'}>
                                            <Offer viewPointHeight={this.viewPointHeight}/>
                                        </Route>
                                    </Scrollbars>

                                </switch>
                            </div>
                        </div>

                    </section>
                </section>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    categoryData: state.category.result,
    isCategoryLoading: state.category.loading,
    bankData: state.bankData.result,
    isBankLoading: state.bankData.loading,
    clientData: state.clientData.result,
    isClientLoading: state.clientData.loading,
});

export default connect(mapStateToProps, {getAllCategory, getAllBank, getAllClient, getAllOffer})(console);