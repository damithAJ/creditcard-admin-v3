import React, {Component, useEffect, useState} from 'react';
import * as authentication from "../../services/authentication";
import {message} from 'antd';

class signIn extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            errors: {},
            isLoading: false,
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.signIn = this.signIn.bind(this);
        this.signIn = this.signIn.bind(this);
        message.config({
            top: 10,
            maxCount: 3,
        });


    }

    componentDidMount() {
    }

    isChangeLoadEvent(_status) {

    }

    //====== HandleInputChange ====
    handleInputChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    //----- sign in ------
    signIn() {
        const user = {
            userName: this.state.email,
            password: this.state.password,
        };
        this.setState({isLoading: true});
        let _thisRef = this;
        authentication.signIn(user, function (state, res) {
            console.log(res);
            _thisRef.setState({isLoading: false});
            if (state) {
                _thisRef.props.history.push("/console");
                message.success('Welcome Back ' + res.data.firstName);
                return true;
            } else {
                message.error('Invalid login details.');
                return false;
            }

        });
    };

    render() {
        return (
            <div className="container">
                <div className="bg"/>
                <div className="sign-in-wrapper">
                    <div className="row justify-content-md-center">

                        <div className="col-lg-4">
                            <div className="logo-header">
                                creditcardoffers.lk
                            </div>
                            <div className="header mt-2 mb-4">
                                <h1 className="mb-5">Sign in</h1>
                            </div>

                            <form noValidate>
                                {/*----- Email address ---------*/}
                                <div className="form-group">
                                    <input
                                        type="email"
                                        placeholder="Email"
                                        className="form-control lg"
                                        name="email"
                                        onChange={this.handleInputChange}
                                        value={this.state.email}
                                    />
                                </div>
                                {/*------ password ------*/}
                                <div className="form-group">
                                    <input
                                        type="password"
                                        placeholder="Password"
                                        className="form-control lg"
                                        name="password"
                                        onChange={this.handleInputChange}
                                        value={this.state.password}/>
                                </div>

                                {/*------ sign-in ------*/}
                                <div className="form-group">
                                    {this.state.isLoading === false &&
                                    <button
                                        onClick={this.signIn}
                                        type="button"
                                        className="btn btn-dark btn-block lg">
                                        Sign In
                                    </button>
                                    }

                                    {this.state.isLoading === true &&
                                    <button
                                        type="button"
                                        disabled
                                        className="btn btn-dark btn-block lg">
                                        loading...
                                    </button>
                                    }
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default signIn;