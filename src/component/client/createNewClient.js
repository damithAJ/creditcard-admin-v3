import React, {useEffect, useState} from "react";
import * as clientApi from "../../services/clientApi";
import {message} from 'antd';
import FileBase64 from 'react-filebase64';
import {Scrollbars} from "react-custom-scrollbars";

const CreateNewClient = props => {
    const [files, setFiles] = useState("");
    const [base64, SetBase64] = useState("");
    const [clientName, setClientName] = useState("");
    const [clientWebUrl, setClientWebUrl] = useState("");
    const [location, setLocation] = useState("");
    const [contactNo, setContactNo] = useState("");
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState("");


    const clearAll = event => {
        setClientName('');
        setClientWebUrl('');
        setLocation('');
        setContactNo('');
        setFiles('');
        SetBase64('');
        setIsLoading(false);
    };


    function validation(callback) {
        if (!clientName) {
            message.error('Invalid client name...');
            setError('clientName');
            callback(true);
            return;
        }
        if (!clientWebUrl) {
            message.error('Invalid web URL...');
            setError('clientWebUrl');
            callback(true);
            return;
        }
        if (!location) {
            message.error('Invalid location details...');
            setError('location');
            callback(true);
            return;
        }
        if (!contactNo) {
            message.error('Invalid contact no...');
            setError('contactNo');
            callback(true);
            return;
        }
        callback(false);
    }

    const saveClient = event => {
        setError('');
        if (files) {
            //validation
            validation(function (state) {
                console.log(state);
                if (!state) {
                    let obj = {
                        'img': base64,
                        'clientName': clientName,
                        'clientWebUrl': clientWebUrl,
                        'location': location,
                        'contactNo': contactNo,
                        'imageId': Math.floor((Math.random() * 108987987987) + 1) + '.jpg',
                        'clientId': Math.floor((Math.random() * 108987987987) + 1).toString(),
                        'lastUpdate': new Date(),
                    };
                    console.log(obj);
                    setIsLoading(true);
                    clientApi.saveClient(obj, function (res) {
                        setIsLoading(false);
                        if (res) {
                            message.success('Client save successfully...');
                            clearAll();
                            props.getAllClient();
                            console.log(res);
                        } else {
                            message.error('Client save unSuccessful...');
                            return false;
                        }
                    });
                }
            });

        } else {
            message.error('Please select bank logo');
            setError('upload');
        }
    };

    // Callback~
    const getFiles = (files) => {
        setError('');
        setFiles(files);
        SetBase64(files.base64);
    };

    return (
        <div>
            <section className="modal-bg-wrapper">
                <div className="container">
                    <div className="row justify-content-md-center">
                        <div className="col-lg-6 col-md-6 col-xl-6 col-sm-12 col-12">
                            <div className="modal-wrapper">
                                {/* header */}
                                <div className="header-wrapper d-flex align-items-center ">
                                    <div className="back-btn"
                                         onClick={(e) => props.openModalPanel('closeMe')}>
                                        <i className="typcn typcn-arrow-left"/>
                                    </div>
                                    <div className="modal-title">
                                        <h2>Create a new client</h2>
                                    </div>
                                </div>
                                {/* body */}
                                <div className="modal-body-wrapper">
                                    <div className="form-wrapper">
                                        <form>
                                            <div>
                                                <Scrollbars style={{height: props.viewPointHeight - 400}}>
                                                    {/*category name*/}
                                                    {/*<div className={`form-group ${errors.category && touched.category && 'is-invalid'}`}>*/}
                                                    <div
                                                        className={`form-group file-upload ${error === 'upload' ? 'is-invalid' : ''}`}>
                                                        <label>Client Logo (200x200) <b
                                                            className="validation">*</b></label>
                                                        <br/>
                                                        <div className="d-flex">
                                                            <div className="uploader float-left">
                                                                <FileBase64
                                                                    multiple={false}
                                                                    onDone={getFiles.bind(this)}/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {/*=== Client name ==== */}
                                                    <div
                                                        className={`form-group ${error === 'clientName' ? 'is-invalid' : ''}`}>
                                                        <label>Client Name <b>*</b></label>
                                                        <input id="clientName"
                                                               name="clientName"
                                                               value={clientName}
                                                               onChange={e => setClientName(e.target.value)}
                                                               className="form-control lg"
                                                               placeholder="Ex : Pitzz Hut"/>
                                                    </div>

                                                    {/*===== client web URL =====*/}
                                                    <div
                                                        className={`form-group ${error === 'clientWebUrl' ? 'is-invalid' : ''}`}>
                                                        <label>Client Website URL <b>*</b></label>
                                                        <input id="clientWebUrl"
                                                               name="clientWebUrl"
                                                               value={clientWebUrl}
                                                               className="form-control lg"
                                                               onChange={e => setClientWebUrl(e.target.value)}
                                                               placeholder="Ex : https://www.pitzz.lk/"/>
                                                    </div>
                                                    {/*===== client location =====*/}
                                                    <div
                                                        className={`form-group ${error === 'location' ? 'is-invalid' : ''}`}>
                                                        <label>Location (outlet location)<b>*</b></label>
                                                        <input id="location"
                                                               name="location"
                                                               value={location}
                                                               className="form-control lg"
                                                               onChange={e => setLocation(e.target.value)}
                                                               placeholder="Ex : colombo 5 | rathnapura"/>
                                                    </div>

                                                    {/*===== client contact no =====*/}
                                                    <div
                                                        className={`form-group ${error === 'contactNo' ? 'is-invalid' : ''}`}>
                                                        <label>Contact No <b>*</b></label>
                                                        <input id="contactNo"
                                                               value={contactNo}
                                                               className="form-control lg"
                                                               onChange={e => setContactNo(e.target.value)}
                                                               placeholder="Ex : +94 715 986 461"/>
                                                    </div>


                                                </Scrollbars>
                                            </div>
                                            <div className="btn-wrapper d-flex align-items-baseline">
                                                {!isLoading &&
                                                <button type="button"
                                                        onClick={() => saveClient()}
                                                        className="btn lg btn-block btn-dark">
                                                    SAVE
                                                </button>
                                                }
                                                {isLoading &&
                                                <button type="button"
                                                        className="btn lg btn-block btn-disable">
                                                    Loading...
                                                </button>
                                                }


                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
};


export default CreateNewClient;