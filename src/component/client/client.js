import React, {Component} from 'react';
import * as authentication from "../../services/authentication";
import * as config from "../../config/serverIP";
import {message} from 'antd';
import {connect} from 'react-redux';
import {getAllClient} from "../../services/clientApi"
import Moment from 'react-moment';

class Client extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        //on load get all category with out pagination
        //todo
        this.props.getAllClient();
    }


    render() {
        const {clientData = [], isLoading} = this.props;
        const row = clientData.reverse().map((d, key) =>
            <tr key={key}>
                <td>
                    <div className="bank-img-wrp sm">
                        <img className="bank-logo"
                             src={config.IMAGE_URL + d.imageId}/>
                    </div>
                </td>
                <td>{d.clientId}</td>

                <td>{d.clientName}</td>
                <td>
                    {d.clientWebUrl.length < 30
                        ? `${d.clientWebUrl}`
                        : `${d.clientWebUrl.substring(0, 30)}...`}
                </td>
                <td>
                    {d.location.length < 30
                        ? `${d.location}`
                        : `${d.location.substring(0, 30)}...`}
                </td>
                <td>
                    <Moment fromNow
                            date={d.lastUpdate}/>
                </td>
                {/*<td>{d.userType}</td>*/}
            </tr>
        );

        return (
            <div>
                <div
                    className={`loading-wrapper  align-items-center" ${isLoading ? 'active' : 'unload'}`}>
                    Loading...
                </div>

                {clientData.length !== 0 &&
                <table className="table theme-light">
                    <thead>
                    <tr>
                        <th scope="col"></th>
                        <th scope="col">Id</th>
                        <th scope="col">Client Name</th>
                        <th scope="col">Web URL</th>
                        <th scope="col">Location</th>
                        <th scope="col">Last updated</th>
                        {/*<th scope="col">Create</th>*/}
                    </tr>
                    </thead>
                    <tbody>
                    {row}
                    </tbody>
                </table>
                }
            </div>
        )
    }
}

Client.propTypes = {};

const mapStateToProps = state => ({
    clientData: state.clientData.result,
    isLoading: state.clientData.loading,
});

export default connect(mapStateToProps, {getAllClient})(Client);