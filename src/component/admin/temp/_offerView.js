import React, {useEffect, useState} from "react";
import * as config from "../../../config/serverIP";
import {message} from 'antd';
import {connect} from 'react-redux';

import Moment from 'react-moment';
import * as authentication from "../../../services/authentication";
import {Scrollbars} from "react-custom-scrollbars";
import ReactHtmlParser from 'react-html-parser';

const OfferView = props => {


    const [error, setError] = useState("");
    const [isSaving, setIsSaving] = useState(false);
    const [viewMore, setViewMore] = useState(false);




    const post = props.selectedOfferDetails;


    return (
        <div>
            {/*====== small view ======*/}
            {viewMore === false &&
            <section className="modal-bg-wrapper">
                <div className="container">
                    <div className="row justify-content-md-center">
                        <div className="col-lg-9 col-md-9 col-xl-9 col-sm-12 col-9">
                            <div className="modal-wrapper">
                                {/* header */}
                                <div className="header-wrapper d-flex align-items-center ">
                                    <div className="back-btn"
                                         onClick={props.closeViewMore}>
                                        <i className="typcn typcn-arrow-left"/>
                                    </div>
                                    {/*==== modal title ===*/}
                                    <div className="modal-title d-flex justify-content-between">
                                        <div className="d-flex flex-column">
                                            <h2>Waiting for approval</h2>
                                            <a href={props.selectedOfferDetails.referenceURL} target='_blank'>
                                                {props.selectedOfferDetails.referenceURL}
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                {/*==== body wrapper =====*/}
                                <div key={props.selectedOfferDetails.offerId}
                                     className="offer-card-wrapper ">
                                    <div className="card-wrapper">
                                        {/*card header*/}
                                        <div className="offer-card-header d-flex ">
                                            <div className="card-title-wrp">
                                                <div className="c-label">{props.selectedOfferDetails.label}</div>
                                                <div
                                                    className="card-title">{props.selectedOfferDetails.header}</div>
                                            </div>
                                        </div>
                                        {/*card body*/}
                                        <div className="card-body-wrapper d-flex">
                                            <div className="offer-image-wrp">
                                                <div className="img-wrp">
                                                    <img src={config.IMAGE_URL + props.selectedOfferDetails.imageId}/>
                                                </div>
                                            </div>
                                            <div className="offer-details-wrp">
                                                <div className="offer-expire">
                                                    {props.selectedOfferDetails.offerDateType === 'to' &&
                                                    <>
                                                        offer valid <Moment
                                                        format=" Do  MMM YYYY">{props.selectedOfferDetails.endDate}</Moment> to <Moment
                                                        format=" Do  MMM YYYY">{props.selectedOfferDetails.offerEnd}</Moment>
                                                    </>
                                                    }
                                                    {props.selectedOfferDetails.offerDateType === 'until' &&
                                                    <>
                                                        offer valid until <Moment
                                                        format=" Do  MMMM YYYY">{props.selectedOfferDetails.offerEnd}</Moment>
                                                    </>
                                                    }
                                                </div>
                                                <div className="offer-location-wrp d-flex">
                                                    <div className="icon-wrapper">
                                                        <i className="feather icon-map-pin"></i>
                                                    </div>
                                                    <div className="offer-location flex-column">
                                                        <div className="c-label">Location</div>
                                                        <div className=" d-flex">
                                                            {
                                                                props.selectedOfferDetails.location.length === 1 &&
                                                                <div
                                                                    className="location-title">{props.selectedOfferDetails.location[0]}</div>
                                                            }

                                                            {
                                                                props.selectedOfferDetails.location.length >= 2 &&
                                                                <div
                                                                    className="location-title">{props.selectedOfferDetails.location[0]} +</div>

                                                            }

                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="offer-location-wrp d-flex">
                                                    <div className="icon-wrapper">
                                                        <i className="feather icon-phone"></i>
                                                    </div>
                                                    <div className="offer-location">
                                                        <div className="c-label">Contact No</div>
                                                        <div
                                                            className="location-title">{props.selectedOfferDetails.client.contactNo}</div>
                                                    </div>
                                                </div>

                                                <div className="web-site-wrp d-flex ml-0">
                                                    <div
                                                        className="view-more text-center d-flex justify-content-center"
                                                        id={props.selectedOfferDetails.offerId}
                                                        onClick={e => setViewMore(true)}>View more
                                                    </div>
                                                    <div
                                                        className="view-more accept text-center d-flex justify-content-center ml-1"
                                                        id={props.selectedOfferDetails.offerId}
                                                        onClick={e => setViewMore(true)}>Accept
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        {/* ====== offer card
                        category
                        views
                        interest
                        bank
                        ====== r*/}

                                        <div className="offer-card-footer d-flex ">
                                            {/*Category*/}
                                            <div className="footer-item flex-column mr-2">
                                                <div className="c-label">Category</div>
                                                <div
                                                    className="title">{props.selectedOfferDetails.category.category}
                                                </div>
                                            </div>
                                            <div className="footer-item flex-column mr-2">
                                                <div className="c-label">Views</div>
                                                <div className="title">{props.selectedOfferDetails.views}</div>
                                            </div>


                                            {/*offer rate ant etc*/}
                                            {props.selectedOfferDetails.offerType === 'off' &&
                                            <div className="footer-item with-bg-yellow mr-2">
                                                <div className="title white">{props.selectedOfferDetails.rate}%
                                                    off
                                                </div>
                                            </div>
                                            }
                                            {props.selectedOfferDetails.offerType === 'upTo' &&
                                            <div className="footer-item with-bg-yellow mr-2">
                                                <div className="title white">Up To {props.selectedOfferDetails.rate}%
                                                    off
                                                </div>
                                            </div>
                                            }
                                            {props.selectedOfferDetails.offerType === 'byOneGetOne' &&
                                            <div className="footer-item with-bg-yellow mr-2">
                                                <div className="title white">Buy One Get One Free</div>
                                            </div>
                                            }
                                            {props.selectedOfferDetails.offerType === 'installmentPlan' &&
                                            <div className="footer-item with-bg-yellow mr-2">
                                                <div className="title white">Installment plans</div>
                                            </div>
                                            }


                                            {/* ====== end interested function start here ===== */}

                                            <div
                                                className={`footer-item with-icon d-flex  mr-2 ${props.selectedOfferDetails.bank.className}`}>
                                                <div className="left-title">
                                                    <div className="c-label">Bank</div>
                                                    <div
                                                        className="title ">{props.selectedOfferDetails.bank.bankName}</div>
                                                </div>
                                                <div className="right-img-wrp">
                                                    <div className="right-img">
                                                        <img style={{height: 32}}
                                                             src={config.IMAGE_URL + props.selectedOfferDetails.bank.imageId}/>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        {/*//end offer card*/}
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </section>
            }

            {/*====== larger view ======*/}

            {viewMore === true &&
            <>
                <section className="modal-bg-wrapper">
                    <div className="container">
                        <div className="row justify-content-md-center">
                            <div className="col-lg-12col-md-12 col-xl-12 col-sm-12 col-12">
                                <div className="modal-wrapper">
                                    <div className="offer-wrapper ">
                                        <Scrollbars
                                            autoHide
                                            disableHorizontalScrolling
                                            style={{height: props.viewPointHeight - 150}}>
                                            <div key={post.offerId}>
                                                <div className="bank-and-client-wrapper d-flex ">
                                                    <div className="client-wrapper desktop">
                                                        <div className="logo-wrp">
                                                            <img
                                                                src={config.IMAGE_URL + post.bank.imageId}/>
                                                        </div>
                                                        <div className={`bank-details ${post.bank.className}`}>
                                                            <div className="title offer-view-title">Bank Name</div>
                                                            <div className="body b">{post.bank.bankName}</div>
                                                        </div>
                                                        <div className={`bank-details ${post.bank.className}`}>
                                                            <div className="title offer-view-title">Website</div>
                                                            <div className="body b">{post.bank.bankUrl}</div>
                                                        </div>


                                                        <div className="apply-card-wrp">
                                                            <a href={post.bank.bankCardUrl}
                                                               target="_blank"
                                                               className="btn-apply-card">
                                                                Apply card
                                                            </a>
                                                        </div>


                                                    </div>
                                                    <div className="offer-view-wrapper desktop">
                                                        {/*post header*/}
                                                        <div className="header-wrp d-flex justify-content-between">
                                                            <div className="header">
                                                                <div
                                                                    className="title offer-view-title">{post.label}</div>
                                                                <div className="body">
                                                                    <h1>{post.header}</h1>
                                                                </div>
                                                            </div>
                                                            <div className="header-wrp">
                                                                <div className="header">
                                                                    <div
                                                                        className="title offer-view-title text-right">Category
                                                                    </div>
                                                                    <div
                                                                        className="body sm text-right">{post.category.category}</div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        {/*post image*/}
                                                        <div className="offer-image-banner desktop">
                                                            <div className="banner-img-wrp">
                                                                <img className="img-banner"
                                                                     src={config.IMAGE_URL + post.imageId}/>
                                                                <div className="client-logo-wrp">
                                                                    <img src={config.IMAGE_URL + post.client.imageId}/>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        {/*post valid location*/}
                                                        <div
                                                            className="offer-location-wrp d-flex justify-content-between desktop">
                                                            <div className="location-list">
                                                                <div className="header-wrp">
                                                                    <div className="header">
                                                                        <div
                                                                            className="title offer-view-title text-left ">Offer
                                                                            valid location
                                                                        </div>
                                                                        <div className="body mt-2 d-flex">
                                                                            {
                                                                                post.location.map(loc => {
                                                                                    return (
                                                                                        <div className="offer-loc">
                                                                                            {loc}
                                                                                        </div>
                                                                                    )
                                                                                })
                                                                            }
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                {/*post valid location*/}
                                                <div className="description-wrapper mt-3 desktop">
                                                    <div className="bank-and-client-wrapper d-flex">
                                                        <div className="client-wrapper">

                                                            <div className="bank-details">
                                                                <div className="title offer-view-title">Offer</div>
                                                                <div className="body mt-2">
                                                                    {/*offer rate ant etc*/}
                                                                    {post.offerType === 'off' &&
                                                                    <div className="footer-item with-bg-yellow mr-2">
                                                                        <div
                                                                            className="title font-primary-color">{post.rate}%
                                                                            off
                                                                        </div>
                                                                    </div>
                                                                    }
                                                                    {post.offerType === 'upTo' &&
                                                                    <div className="footer-item with-bg-yellow mr-2">
                                                                        <div className="title font-primary-color">Up
                                                                            To {post.rate}% off
                                                                        </div>
                                                                    </div>
                                                                    }

                                                                    {post.offerType === 'byOneGetOne' &&
                                                                    <div className="footer-item with-bg-yellow mr-2">
                                                                        <div className="title font-primary-color">Buy
                                                                            One Get
                                                                            One Free
                                                                        </div>
                                                                    </div>
                                                                    }
                                                                    {post.offerType === 'installmentPlan' &&
                                                                    <div className="footer-item with-bg-yellow mr-2">
                                                                        <div className="title font-primary-color">0%
                                                                            Installment
                                                                            plans
                                                                            for {post.rate} months
                                                                        </div>
                                                                    </div>
                                                                    }

                                                                </div>
                                                            </div>

                                                            <div className="bank-details">
                                                                <div className="title offer-view-title">Offer
                                                                    Description
                                                                </div>
                                                                <div className="body mt-2">
                                                                    {ReactHtmlParser(post.offerDescription)}
                                                                </div>
                                                            </div>


                                                            <div className={`bank-details ${post.bank.className}`}>
                                                                <div className="title offer-view-title">Valid Period
                                                                </div>
                                                                <div className="body validDesc mt-2">
                                                                    {props.selectedOfferDetails.offerDateType === 'to' &&
                                                                    <>
                                                                        offer valid <Moment
                                                                        format=" Do  MMM YYYY">{props.selectedOfferDetails.endDate}</Moment> to <Moment
                                                                        format=" Do  MMM YYYY">{props.selectedOfferDetails.offerEnd}</Moment>
                                                                    </>
                                                                    }
                                                                    {props.selectedOfferDetails.offerDateType === 'until' &&
                                                                    <>
                                                                        offer valid until <Moment
                                                                        format=" Do  MMMM YYYY">{props.selectedOfferDetails.offerEnd}</Moment>
                                                                    </>
                                                                    }
                                                                </div>
                                                            </div>


                                                            <div className="d-flex">
                                                                <div className="bank-details">
                                                                    <div
                                                                        className="title offer-view-title text-left ">End
                                                                        date
                                                                    </div>
                                                                    <div
                                                                        className="body text-left mt-2">{post.offerEnd}</div>
                                                                </div>
                                                            </div>


                                                            <div className="bank-details">
                                                                <div className="title offer-view-title text-left ">Terms
                                                                    &
                                                                    Conditions
                                                                </div>


                                                                {
                                                                    post.termsConditions.map(data => {
                                                                        return (
                                                                            <div className="d-flex  mt-2 ">
                                                                                <div className="check-icon-wrp">
                                                                                    <div className="icon mr-2">
                                                                                        <i className="feather icon-check"></i>
                                                                                    </div>
                                                                                </div>
                                                                                <div
                                                                                    className="body text-left">{data}</div>
                                                                            </div>
                                                                        )
                                                                    })
                                                                }


                                                            </div>


                                                        </div>
                                                    </div>
                                                </div>

                                                {/* share social media end */}
                                                <div className="description-wrapper mt-3 desktop">
                                                    <div className="bank-and-client-wrapper d-flex flex-row-reverse">
                                                        <div className="back-to-wrp">
                                                            <div className="animated slideInRight home-link-footer">
                                                                <div className="home-link-wrapper d-flex"
                                                                     onClick={e => setViewMore(false)}>
                                                                    <div className="icon">
                                                                        <i className="feather icon-arrow-left"></i>
                                                                    </div>
                                                                    <div className="text">Back to home</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {/* share social media end */}
                                            </div>
                                        </Scrollbars>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>
            </>
            }
        </div>
    )
};


export default OfferView;