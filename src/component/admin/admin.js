import React, {Component} from 'react';
import * as authentication from "../../services/authentication";
import * as offerService from "../../services/offerApi";
import {message} from 'antd';
import {connect} from 'react-redux';
import {getAllBank} from "../../services/bankApi"
import Moment from 'react-moment';
import {isLogin, userDetails} from "../../utils";
import {Scrollbars} from "react-custom-scrollbars";
import * as offerApi from "../../services/offerApi";


import OfferView from "./temp/_offerView";

class Admin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            token: null,
            viewPointWidth: 0,
            viewPointHeight: 0,
            isLoginStatus: false,
            animated: false,
            userObj: null,

            pageNo: 1,
            pageSize: 10,
            showLoadingMore: true,
            offerData: [],
            isLoadingOfferData: false,
            isLoadingPageDate: false,
            selectedOfferDetails: null,
            isOpenOfferView: false,
            isAccepting: false
        };
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
        this.viewMoreAboutOffer = this.viewMoreAboutOffer.bind(this);
        this.closeViewMore = this.closeViewMore.bind(this);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleOnScroll);
        window.removeEventListener('resize', this.updateWindowDimensions);
        window.addEventListener('scroll', this.listenToScroll);
    }


    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
        window.addEventListener('scroll', this.handleOnScroll);
        // ======= get all offers
        this.setState({
            isLoadingOfferData: true,
            isLoadingPageDate: false
        });
        offerService.getOfferByPageId(this.state.pageNo, this.state.pageSize, true)
            .then(offer => {
                console.log(offer.data.result);
                this.setState({
                    offerData: offer.data.result,
                    isLoadingOfferData: false
                });
            }).catch(ex => {
            this.setState({
                isLoadingOfferData: false
            });
        });

        // var userObj = userDetails();
        // this.setState({userObj: userObj});


        this.setState({
            isLoginStatus: isLogin()
        });
        setTimeout(() => {
            this.setState(() => {
                return {animated: true}
            })
        }, this.props.delay)
    }

    updateWindowDimensions() {
        this.setState({viewPointWidth: window.innerWidth, viewPointHeight: window.innerHeight});
    }

    viewMoreAboutOffer = (offer) => {
        console.log(offer);
        this.setState({
            selectedOfferDetails: offer,
            isOpenOfferView: !this.state.isOpenOfferView
        });
    };

    closeViewMore() {
        this.setState({
            selectedOfferDetails: null,
            isOpenOfferView: false
        });
    };

    //========= accept offer
    acceptOffer = (offer) => {
        let jsonObj = {
            'offerId': offer.offerId,
            'categoryId': offer.category.categoryId,
            'clientId': offer.client.clientId,
            'bankId': offer.bank.bankId,
            'label': offer.label,
            'header': offer.header,
            'offerDateType': offer.offerDateType,
            'offerStart': offer.offerStart,
            'offerEnd': offer.offerEnd,
            'customizeDate': offer.customizeDate,
            'validDescription': 'offer valid',
            'cardType': offer.cardType,
            'offerType': offer.offerType,
            'rate': offer.rate,
            'contactNo': offer.client.contactNo,
            'location': offer.location,
            'offerDescription': offer.offerDescription,
            'termsConditions': offer.termsConditions,
            'referenceURL': offer.referenceURL,
            'imageId': offer.imageId,
            'views': 0,
            'interest': 0,
            'status': true,
            'userId': 0,
            'userType': 0
        };

        this.setState({
            isAccepting: true
        });
        let _this = this;
        offerApi.acceptOffer(jsonObj, function (d) {
            _this.setState({
                isAccepting: false
            });
            if (d) {
                message.success('Offer accepted..')
            } else {
                message.error('Offer accepted error..')
            }
        })
    };


    render() {
        const {offerData = [], isLoadingOfferData, selectedOfferDetails, viewPointHeight, isAccepting} = this.state;
        // console.log(offerData);
        const offerCard = offerData.reverse().map((d, key) =>
            <div className="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                <div className="row">
                    <div key={key} className="pending-offer-card">
                        <div className="p-offer-header">
                            <div className="title">
                                {d.label}
                            </div>
                            <div className="header">
                                {d.header}
                            </div>
                        </div>

                        <div className="p-offer-banner">
                            <img className="bank-logo img-fluid"
                                 alt="Responsive image"
                                 src={'https://creditcardoffers.s3-ap-southeast-1.amazonaws.com/media/' + d.imageId}/>
                        </div>

                        <div className="category-bank-wrapper d-flex">
                            <div className="category-wrp d-flex flex-column text-left">
                                <div className="title text-left">Category</div>
                                <div className="category text-left">
                                    {d.category.category}
                                </div>
                            </div>
                            <div
                                className={`category-wrp d-flex flex-column text-left bank-details `}>
                                <div className="title text-left">Bank</div>
                                <div className={`category text-left bank-text ${d.bank.className}`}>
                                    {d.bank.bankName}
                                </div>
                            </div>
                        </div>

                        <div className="category-bank-wrapper d-flex mt-2">
                            <div className="category-wrp d-flex flex-column text-left">
                                <div className="title text-left">start & end date</div>
                                <div className="category text-left">
                                    {d.offerDateType === 'customize' &&
                                    <>
                                        {d.customizeDate}
                                    </>
                                    }
                                    {d.offerDateType !== 'customize' &&
                                    <>
                                        {d.offerStart} {d.offerDateType} {d.offerEnd}
                                    </>
                                    }
                                </div>
                            </div>
                        </div>

                        <div className="offer-btn-wrapper d-flex  flex-row-reverse">
                            <button type="button" className="btn sm  btn-dark">
                                <div className="d-flex align-items-center"
                                     onClick={event => this.viewMoreAboutOffer(d)}>
                                    <div className="title"> More</div>
                                </div>
                            </button>
                            <button type="button" className="btn sm   mr-4">
                                {isAccepting === false &&
                                <div className="d-flex align-items-center"
                                     onClick={event => this.acceptOffer(d)}>
                                    <div className="title">Accept</div>
                                </div>
                                }
                                {isAccepting === true &&
                                <div className="d-flex align-items-center">
                                    <div className="title">accepting...</div>
                                </div>
                                }
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );

        return (
            <div>
                <div
                    className={`loading-wrapper  align-items-center" ${isLoadingOfferData ? 'active' : 'unload'}`}>
                    Loading...
                </div>

                {selectedOfferDetails &&
                <OfferView
                    closeViewMore={this.closeViewMore}
                    viewPointHeight={viewPointHeight}
                    selectedOfferDetails={selectedOfferDetails}/>
                }

                <Scrollbars
                    disableHorizontalScrolling
                    renderTrackHorizontal={props => <div {...props} style={{display: 'none'}}
                                                         className="track-horizontal"/>}
                    style={{height: this.state.viewPointHeight - 120}}>
                    <div className="row pl-3 pr-3">
                        {offerCard}
                    </div>
                </Scrollbars>

            </div>
        )
    }
}

Admin.propTypes = {};

const mapStateToProps = state => ({
    bankData: state.bankData.result,
    isLoading: state.bankData.loading,
});

export default connect(mapStateToProps, {getAllBank})(Admin);