import React, {useEffect, useState} from "react";
import * as bankApi from "../../services/bankApi";
import {message} from 'antd';
import {connect} from 'react-redux';

import Moment from 'react-moment';
import FileBase64 from 'react-filebase64';
import Bank from "./bank";
import {Scrollbars} from "react-custom-scrollbars";

const CreateNewBank = props => {
    const [files, setFiles] = useState("");
    const [base64, SetBase64] = useState("");
    const [bankName, setBankName] = useState("");
    const [bankUrl, setBankUrl] = useState("");
    const [bankCardUrl, setBankCardUrl] = useState("");
    const [className, setClassName] = useState("");
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState("");


    const clearAll = event => {
        setBankName('');
        setBankUrl('');
        setBankCardUrl('');
        setClassName('');
        setFiles('');
        SetBase64('');
        setIsLoading(false);
    };


    function validation(callback) {
        if (!bankName) {
            message.error('Invalid bank name...');
            setError('bankName');
            callback(true);
            return;
        }
        if (!bankUrl) {
            message.error('Invalid bank URL...');
            setError('bankUrl');
            callback(true);
            return;
        }
        if (!bankCardUrl) {
            message.error('Invalid card URL...');
            setError('bankCardUrl');
            callback(true);
            return;
        }
        if (!className) {
            message.error('Invalid classname...');
            setError('className');
            callback(true);
            return;
        }
        callback(false);
    }

    const saveBank = event => {
        setError('');
        if (files) {
            //validation
            validation(function (state) {
                console.log(state);
                if (!state) {
                    let obj = {
                        'img': base64,
                        'bankName': bankName,
                        'bankUrl': bankUrl,
                        'bankCardUrl': bankCardUrl,
                        'className': className,
                        'imageId': Math.floor((Math.random() * 108987987987) + 1) + '.jpg',
                        'bankId': Math.floor((Math.random() * 108987987987) + 1).toString(),
                        'lastUpdate': new Date(),
                    };
                    console.log(obj);
                    setIsLoading(true);
                    bankApi.saveBank(obj, function (res) {
                        setIsLoading(false);
                        if (res) {
                            message.success('Bank save successfully...');
                            clearAll();
                            props.getAllBank();
                            console.log(res);
                        } else {
                            message.error('Bank save unSuccessful...');
                            return false;
                        }
                    });
                }
            });

        } else {
            message.error('Please select bank logo');
            setError('upload');
        }
    };

    // Callback~
    const getFiles = (files) => {
        setError('');
        setFiles(files);
        SetBase64(files.base64);
    };

    return (
        <div>
            <section className="modal-bg-wrapper">
                <div className="container">
                    <div className="row justify-content-md-center">
                        <div className="col-lg-6 col-md-6 col-xl-6 col-sm-12 col-12">
                            <div className="modal-wrapper">
                                {/* header */}
                                <div className="header-wrapper d-flex align-items-center ">
                                    <div className="back-btn"
                                         onClick={(e) => props.openModalPanel('closeMe')}>
                                        <i className="typcn typcn-arrow-left"/>
                                    </div>
                                    <div className="modal-title">
                                        <h2>Create a new bank</h2>
                                    </div>
                                </div>
                                {/* body */}
                                <div className="modal-body-wrapper">
                                    <div className="form-wrapper">
                                        <form>
                                            <div>
                                                <Scrollbars style={{height: props.viewPointHeight - 400}}>
                                                    {/*category name*/}
                                                    {/*<div className={`form-group ${errors.category && touched.category && 'is-invalid'}`}>*/}
                                                    <div
                                                        className={`form-group file-upload ${error === 'upload' ? 'is-invalid' : ''}`}>
                                                        <label>Bank Logo (200x200) <b className="validation">*</b></label>
                                                        <br/>
                                                        <div className="d-flex">
                                                            <div className="uploader float-left">
                                                                <FileBase64
                                                                    multiple={false}
                                                                    onDone={getFiles.bind(this)}/>
                                                            </div>

                                                            {/*<div className="view float-right">*/}
                                                            {/*    <div className="logo-view">*/}
                                                            {/*        <img src={require('../../assets/img/image.svg')}/>*/}
                                                            {/*    </div>*/}
                                                            {/*</div>*/}
                                                        </div>
                                                    </div>
                                                    {/*=== Bank Name ==== */}
                                                    <div
                                                        className={`form-group ${error === 'bankName' ? 'is-invalid' : ''}`}>
                                                        <label>Bank Name <b>*</b></label>
                                                        <input id="bankName"
                                                               name="bankName"
                                                               value={bankName}
                                                               onChange={e => setBankName(e.target.value)}
                                                               className="form-control lg"
                                                               placeholder="Ex : HSBC Bank"/>
                                                    </div>

                                                    {/*===== bank bank web site url =====*/}
                                                    <div
                                                        className={`form-group ${error === 'bankUrl' ? 'is-invalid' : ''}`}>
                                                        <label>Bank Website URL <b>*</b></label>
                                                        <input id="bankUrl"
                                                               name="bankUrl"
                                                               value={bankUrl}
                                                               className="form-control lg"
                                                               onChange={e => setBankUrl(e.target.value)}
                                                               placeholder="Ex : https://www.hsbc.lk/"/>
                                                    </div>

                                                    {/*===== Card URL (bank card ) =====*/}
                                                    <div
                                                        className={`form-group ${error === 'bankCardUrl' ? 'is-invalid' : ''}`}>
                                                        <label>Card URL (Apply online) <b>*</b></label>
                                                        <input id="bankCardUrl"
                                                               value={bankCardUrl}
                                                               className="form-control lg"
                                                               onChange={e => setBankCardUrl(e.target.value)}
                                                               placeholder="Ex : https://www.hsbc.lk/creditcard"/>
                                                    </div>

                                                    {/*===== Card URL (bank card ) =====*/}
                                                    <div className="form-group">
                                                        <label>Class Name <b>*</b></label>
                                                        <input id="className"
                                                               value={className}
                                                               className="form-control lg"
                                                               onChange={e => setClassName(e.target.value)}
                                                               placeholder="Ex : hsbc"/>
                                                    </div>
                                                </Scrollbars>
                                            </div>
                                            <div className="btn-wrapper d-flex align-items-baseline">
                                                {!isLoading &&
                                                <button type="button"
                                                        onClick={() => saveBank()}
                                                        className="btn lg btn-block btn-dark">
                                                    SAVE
                                                </button>
                                                }
                                                {isLoading &&
                                                <button type="button"
                                                        className="btn lg btn-block btn-disable">
                                                    Loading...
                                                </button>
                                                }


                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
};


export default CreateNewBank;