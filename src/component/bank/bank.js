import React, {Component} from 'react';
import * as authentication from "../../services/authentication";
import {message} from 'antd';
import {connect} from 'react-redux';
import {getAllBank} from "../../services/bankApi"
import Moment from 'react-moment';

class Bank extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        //on load get all category with out pagination
        //todo
        this.props.getAllBank();
    }


    render() {
        const {bankData = [], isLoading} = this.props;
        const row = bankData.reverse().map((d, key) =>
            <tr key={key}>

                <td>
                    <div className="bank-img-wrp sm">
                        <img className="bank-logo"
                             src={'https://creditcardoffers.s3.ap-southeast-1.amazonaws.com/media/' + d.imageId}/>
                    </div>
                </td>
                <td>{d.bankId}</td>

                <td>{d.bankName}</td>
                <td>{d.bankUrl}</td>
                <td>
                    <Moment fromNow
                            date={d.lastUpdate}/>
                </td>
                {/*<td>{d.userType}</td>*/}
            </tr>
        );

        return (
            <div>
                <div
                    className={`loading-wrapper  align-items-center" ${isLoading ? 'active' : 'unload'}`}>
                    Loading...
                </div>

                {bankData.length !== 0 &&
                <table className="table theme-light">
                    <thead>
                    <tr>
                        <th scope="col"></th>
                        <th scope="col">Id</th>
                        <th scope="col">Bank Name</th>
                        <th scope="col">Web URL</th>
                        <th scope="col">Last updated</th>
                        {/*<th scope="col">Create</th>*/}
                    </tr>
                    </thead>
                    <tbody>
                    {row}
                    </tbody>
                </table>
                }
            </div>
        )
    }
}

Bank.propTypes = {};

const mapStateToProps = state => ({
    bankData: state.bankData.result,
    isLoading: state.bankData.loading,
});

export default connect(mapStateToProps, {getAllBank})(Bank);