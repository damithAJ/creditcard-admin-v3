import React, {Component} from 'react';
import * as authentication from "../../services/authentication";
import {message} from 'antd';
import {connect} from 'react-redux';
import {getAllCategory} from "../../services/categoryApi"
import Moment from 'react-moment';

class category extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        //on load get all category with out pagination
        //todo
        this.props.getAllCategory();
    }


    render() {
        const {categoryData = [], isLoading} = this.props;
        const categoryRow = categoryData.reverse().map((d, key) =>
            <tr key={key}>
                <td>{d.categoryId}</td>
                <td>{d.category}</td>
                <td>
                    <Moment fromNow
                            date={d.lastUpdate}/>
                </td>
                <td>{d.userType}</td>
            </tr>
        );

        return (
            <div>
                <div
                    className={`loading-wrapper  align-items-center" ${isLoading ? 'active' : 'unload'}`}>
                    Loading...
                </div>


                {categoryData.length !== 0 &&
                <table className="table theme-light">
                    <thead>
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Category Name</th>
                        <th scope="col">Last updated</th>
                        <th scope="col">Create</th>
                    </tr>
                    </thead>
                    <tbody>
                    {categoryRow}
                    </tbody>
                </table>
                }
            </div>
        )
    }
}

category.propTypes = {};

const mapStateToProps = state => ({
    categoryData: state.category.result,
    isLoading: state.category.loading,
});

export default connect(mapStateToProps, {getAllCategory})(category);