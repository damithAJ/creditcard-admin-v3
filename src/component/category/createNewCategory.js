import React, {useEffect, useState} from "react";
import * as categoryApi from "../../services/categoryApi";
import {message} from 'antd';
import {connect} from 'react-redux';

import Moment from 'react-moment';
import * as authentication from "../../services/authentication";

const CreateNewCategory = props => {
    const [categoryName, setCategoryName] = useState("");
    const [isLoading, setIsLoading] = useState(false);

    const handleChange = event => {
        setCategoryName(event.target.value);
    };

    const saveCategory = event => {
        let obj = {
            'category': categoryName,
            'categoryId': Math.floor((Math.random() * 108987987987) + 1).toString(),
            'lastUpdate': new Date(),
        };
        console.log(obj);
        setIsLoading(true);
        categoryApi.saveCategory(obj, function (res) {
            setIsLoading(false);
            if (res) {
                message.success('Category save successfully...');
                setCategoryName('');
                props.getAllCategory();
                console.log(res);
            } else {
                message.error('Category save unSuccessful...');
            }
        });
    };

    return (
        <div>
            <section className="modal-bg-wrapper">
                <div className="container">
                    <div className="row justify-content-md-center">
                        <div className="col-lg-6 col-md-6 col-xl-6 col-sm-12 col-12">
                            <div className="modal-wrapper">
                                {/* header */}
                                <div className="header-wrapper d-flex align-items-center ">
                                    <div className="back-btn"
                                         onClick={(e) => props.openModalPanel('closeMe')}>
                                        <i className="typcn typcn-arrow-left"/>
                                    </div>
                                    <div className="modal-title">
                                        <h2>Create a new category</h2>
                                    </div>
                                </div>
                                {/* body */}
                                <div className="modal-body-wrapper">
                                    <div className="form-wrapper">
                                        <form>
                                            {/*category name*/}
                                            {/*<div className={`form-group ${errors.category && touched.category && 'is-invalid'}`}>*/}
                                            <div className="form-group">
                                                <label>Category Name <b>*</b></label>
                                                <input id="category"
                                                       name="categoryName"
                                                       value={categoryName}
                                                       onChange={handleChange}
                                                       className="form-control lg"
                                                       placeholder="Ex : fashion & design"/>

                                                <div className="btn-wrapper d-flex align-items-baseline">
                                                    {!isLoading &&
                                                    <button type="button"
                                                            onClick={() => saveCategory()}
                                                            className="btn lg btn-block btn-dark">
                                                        SAVE
                                                    </button>
                                                    }
                                                    {isLoading &&
                                                    <button type="button"
                                                            className="btn lg btn-block btn-disable">
                                                        Loading...
                                                    </button>
                                                    }
                                                </div>

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
};


export default CreateNewCategory;