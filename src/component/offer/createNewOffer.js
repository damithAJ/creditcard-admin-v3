import React, {useEffect, useState} from "react";
import * as offerApi from "../../services/offerApi";
import {message} from 'antd';
import {connect} from 'react-redux';
import Moment from 'react-moment';
import * as authentication from "../../services/authentication";
import {Scrollbars} from "react-custom-scrollbars";

import CreateNewCategory from "../category/createNewCategory";
import CreateNewBank from "../bank/createNewBank";
import CreateNewClient from "../client/createNewClient";

import BankOffer from "./temp/_bank";
import ClientDetails from "./temp/_client";
import CategoryOffer from "./temp/_category";
import OfferDetails from "./temp/_offerDetails";
import LocationAndDescription from "./temp/_locationAndDescription";
import TermsAndConditions from "./temp/_termsAndConditions";
import ReferenceAndImage from "./temp/_referenceAndImage";

const CreateNewOffer = props => {
    const [bankId, SetBankId] = useState(null);
    const [clientId, SetClientId] = useState(null);
    const [label, SetLabel] = useState(null);
    const [header, SetHeader] = useState(null);
    const [categoryId, SetCategoryId] = useState(null);
    const [startDate, SetStartDate] = useState(null);
    const [endDate, SetEndDate] = useState(null);
    const [customizeDate, SetCustomizeDate] = useState(null);
    const [offerDateType, setOfferDateType] = useState('to');
    const [cardType, setCardType] = useState('credit');
    const [offType, setOffType] = useState('off');
    const [offerRate, setOfferRate] = useState(0);
    const [contactNo, setContactNo] = useState('');
    const [location, setLocation] = useState([]);
    const [description, setDescription] = useState(null);
    const [termsConditions, setTermsConditions] = useState([]);
    const [terms, setTerms] = useState(null);
    const [reference, setReference] = useState(null);
    const [imageId, setImagId] = useState(null);

    const [files, setFiles] = useState(null);
    const [base64, setBase64] = useState(null);

    const [wizardNo, setWizardNo] = useState(1);
    const [isLoading, setIsLoading] = useState(false);


    const [error, setError] = useState("");

    const [isSaving, setIsSaving] = useState(false);

    const [openModalName, setOpenModalName] = useState(null);


    //clear all
    function clearAll() {
        SetBankId(null);
        SetClientId(null);
        SetLabel(null);
        SetHeader(null);
        SetCategoryId(null);
        SetStartDate(null);
        SetEndDate(null);
        SetCustomizeDate(null);
        setOfferDateType('to');
        setCardType('credit');
        setOffType('off');
        setOfferRate(null);
        setOfferRate(null);
        setContactNo(null);
        setLocation([]);
        setDescription(null);
        setTermsConditions([]);
        setTerms(null);
        setReference(null);
        setImagId(null);
        setFiles(null);
        setBase64(null);
        setWizardNo(1);
        setError(null);
        setError(false);
    }

    //goto next to wizard
    function gotoWizard(_wizard) {
        setError(null);
        switch (_wizard) {
            case 1:
                setWizardNo(1);
                break;
            case 2:
                if (categoryId !== null) {
                    setWizardNo(2);
                    if (props.bankData && props.bankData.length === 0) {
                        props.getAllBank();
                    }
                } else {
                    message.error('please select category');
                }
                break;
            case 3:
                if (bankId !== null) {
                    setWizardNo(3);
                    if (props.clientData && props.clientData.length === 0) {
                        props.getAllClient();
                    }
                } else {
                    message.error('please select bank');
                }
                break;
            case 4:
                if (clientId !== null) {
                    setWizardNo(4);
                } else {
                    message.error('please select client');
                }
                break;
            case 5:
                if (label === null) {
                    setError('label');
                    message.error('please enter  offer title');
                    return;
                }
                if (header === null) {
                    setError('header');
                    message.error('please enter  offer header');
                    return;
                }

                if (offerDateType === 'customize') {
                    if (customizeDate === null) {
                        setError('customize');
                        message.error('please select customize date');
                        return;
                    }
                }
                if (offerDateType === 'to' || offerDateType === 'until' ) {
                    if (startDate === null && endDate === null) {
                        setError('dateRange');
                        message.error('please select  offer start & end date');
                        return;
                    }
                }

                if (offerRate === 0) {
                    setError('offerRate');
                    message.error('please enter offer rate');
                    return;
                }
                setWizardNo(5);
                break;
            case 6:
                if (location.length === 0) {
                    setError('location');
                    message.error('please select  valid location.');
                    return;
                }
                if (description === null) {
                    setError('description');
                    message.error('please enter  description.');
                    return;
                }
                setWizardNo(6);
                break;
            case 7:
                if (termsConditions.length === 0) {
                    setError('terms');
                    message.error('please enter terms Conditions.');
                    return;
                }
                setWizardNo(7);
                break;
            default:
                break;
        }
    }

    //goto back to wizard
    function goBackToWizard(_wizard) {
        setError(null);
        switch (_wizard) {
            case 1:
                if (props.categoryData && props.categoryData.length === 0) {
                    props.getAllCategory();
                }
                setWizardNo(1);
                break;
            case 2:
                setWizardNo(2);
                break;
            case 3:
                setWizardNo(3);
                break;
            case 4:
                setWizardNo(4);
                break;
            case 5:
                setWizardNo(5);
                break;
            case 6:
                setWizardNo(6);
                break;
            case 7:
                setWizardNo(7);
                break;
            default:
                break;
        }
    }

    //save offer
    const saveOffer = event => {
        setError(null);
        if (reference === null) {
            setError('reference');
            message.error("Please enter offer Reference URL.");
            return;
        }
        if (files === null) {
            setError('upload');
            message.error('Please select banner image');
            return;
        }


        setIsSaving(true);
        const jsonObj = {
            'offerId': Math.floor((Math.random() * 108987987987) + 1).toString(),
            'categoryId': categoryId,
            'clientId': clientId,
            'bankId': bankId,
            'label': label,
            'header': header,
            'offerDateType': offerDateType,
            'offerStart': startDate,
            'offerEnd': endDate,
            'customizeDate': customizeDate,
            'validDescription': 'offer valid',
            'cardType': cardType,
            'offerType': offType,
            'rate': offerRate,
            'contactNo': contactNo,
            'location': location,
            'offerDescription': description,
            'termsConditions': termsConditions,
            'referenceURL': reference,
            'imageId': Math.floor((Math.random() * 108987987987) + 1) + '.jpg',
            'img': base64,
            'views': 0,
            'interest': 0,
            'status': false,
        };

        offerApi.saveOffer(jsonObj, function (d) {
            clearAll();
            if (d) {
                message.success('Offer save Successful..')
            } else {
                message.error('Offer save unsuccessful..')
            }

        })
    };

    const handleScroll = event => {
        console.log(event);
    };

    //create new item
    function openModalPanel(_action) {
        switch (_action) {
            case 'newCategory':
                //create new category
                setOpenModalName(_action);
                break;
            case 'newBank':
                //create new bank
                setOpenModalName(_action);
                break;
            case 'newClient':
                //create new client
                setOpenModalName(_action);
                break;
            case 'closeMe':
                setOpenModalName(_action);
                break;
            default:
                break;
        }
    }

    return (
        <div>
            <section className="modal-bg-wrapper">
                <div className="container">
                    <div className="row justify-content-md-center">
                        <div className="col-lg-6 col-md-7 col-xl-6 col-sm-12 col-12">
                            <div className="modal-wrapper">
                                {/* header */}
                                <div className="header-wrapper d-flex align-items-center ">
                                    <div className="back-btn"
                                         onClick={(e) => props.openModalPanel('closeMe')}>
                                        <i className="typcn typcn-delete-outline"/>
                                    </div>
                                    {/*==== modal title ===*/}
                                    <div className="modal-title d-flex justify-content-between">
                                        <div className="d-flex flex-column">
                                            <h2>Create a new offer </h2>
                                            {wizardNo === 1 &&
                                            <small>Select Category</small>
                                            }
                                            {wizardNo === 2 &&
                                            <small>Select Bank</small>
                                            }
                                            {wizardNo === 3 &&
                                            <small>Select Client</small>
                                            }
                                            {wizardNo === 4 &&
                                            <small>Offer Details</small>
                                            }
                                            {wizardNo === 5 &&
                                            <small>Location & Description</small>
                                            }
                                            {wizardNo === 6 &&
                                            <small>Terms And Conditions</small>
                                            }
                                            {wizardNo === 7 &&
                                            <small>Banner & URL </small>
                                            }
                                        </div>

                                        {/*==== create new item inside the offer form ======*/}
                                        <div>
                                            {/*==== create new category =====*/}
                                            {wizardNo === 1 &&
                                            <div className="create-new float-right justify-content-end"
                                                 onClick={(e) => openModalPanel('newCategory')}>
                                                Create New Category
                                            </div>
                                            }
                                            {/*==== create new bank =====*/}
                                            {wizardNo === 2 &&
                                            <div className="create-new float-right justify-content-end"
                                                 onClick={(e) => openModalPanel('newBank')}>
                                                Create New Bank
                                            </div>
                                            }
                                            {/*==== create new client =====*/}
                                            {wizardNo === 3 &&
                                            <div className="create-new float-right justify-content-end"
                                                 onClick={(e) => openModalPanel('newClient')}>
                                                Create New Client
                                            </div>
                                            }
                                        </div>
                                    </div>
                                </div>


                                {/*======== offer create wizard ====== */}
                                <div className="modal-body-wrapper">
                                    <div className="form-wrapper auto-scroller m1-t">
                                        <Scrollbars
                                            onScroll={handleScroll}
                                            autoHide
                                            disableHorizontalScrolling
                                            style={{height: props.viewPointHeight - 350}}>

                                            {/*=== create new item =======*/}
                                            {openModalName === 'newCategory' &&
                                            <CreateNewCategory
                                                getAllCategory={props.getAllCategory}
                                                openModalPanel={openModalPanel}/>
                                            }
                                            {openModalName === 'newBank' &&
                                            <CreateNewBank
                                                getAllBank={props.getAllBank}
                                                viewPointHeight={props.viewPointHeight}
                                                openModalPanel={openModalPanel}/>
                                            }
                                            {openModalName === 'newClient' &&
                                            <CreateNewClient
                                                viewPointHeight={props.viewPointHeight}
                                                getAllClient={props.getAllClient}
                                                openModalPanel={openModalPanel}/>
                                            }

                                            {/*=== select offer category  ======*/}
                                            {wizardNo === 1 &&
                                            <CategoryOffer
                                                categoryData={props.categoryData}
                                                categoryId={categoryId}
                                                isCategoryLoading={props.isCategoryLoading}
                                                SetCategoryId={SetCategoryId}
                                                viewPointHeight={props.viewPointHeight}/>}

                                            {/*=== select offer bank  ======*/}
                                            {wizardNo === 2 &&
                                            <BankOffer
                                                bankData={props.bankData}
                                                bankId={bankId}
                                                SetBankId={SetBankId}
                                                isBankLoading={props.isBankLoading}
                                                viewPointHeight={props.viewPointHeight}/>}

                                            {/*=== select client  ======*/}
                                            {wizardNo === 3 &&
                                            <ClientDetails
                                                clientId={clientId}
                                                clientData={props.clientData}
                                                isClientLoading={props.isClientLoading}
                                                SetClientId={SetClientId}
                                                viewPointHeight={props.viewPointHeight}/>}

                                            {/*=== offer details ======*/}
                                            {wizardNo === 4 &&
                                            <OfferDetails
                                                label={label}
                                                setLabel={SetLabel}
                                                header={header}
                                                SetHeader={SetHeader}
                                                startDate={startDate}
                                                offerDateType={offerDateType}
                                                setOfferDateType={setOfferDateType}
                                                SetCustomizeDate={SetCustomizeDate}
                                                customizeDate={customizeDate}
                                                SetStartDate={SetStartDate}
                                                setCardType={setCardType}
                                                cardType={cardType}
                                                offType={offType}
                                                setOffType={setOffType}
                                                endDate={endDate}
                                                SetEndDate={SetEndDate}
                                                offerRate={offerRate}
                                                setOfferRate={setOfferRate}
                                                setContactNo={setContactNo}
                                                contactNo={contactNo}
                                                error={error}
                                                viewPointHeight={props.viewPointHeight}/>}
                                            {wizardNo === 5 &&
                                            <LocationAndDescription
                                                setLocation={setLocation}
                                                location={location}
                                                description={description}
                                                error={error}
                                                setDescription={setDescription}
                                            />}
                                            {wizardNo === 6 &&
                                            <TermsAndConditions
                                                setTermsConditions={setTermsConditions}
                                                viewPointHeight={props.viewPointHeight}
                                                termsConditions={termsConditions}
                                                setTerms={setTerms}
                                                error={error}
                                                terms={terms}
                                            />}
                                            {wizardNo === 7 &&
                                            <ReferenceAndImage
                                                viewPointHeight={props.viewPointHeight}
                                                setReference={setReference}
                                                reference={reference}
                                                setImagId={setImagId}
                                                setError={setError}
                                                setFiles={setFiles}
                                                setBase64={setBase64}
                                                error={error}
                                                imagId={imageId}/>
                                            }
                                        </Scrollbars>

                                        <div
                                            className="btn-wrapper d-flex align-items-baseline flex-row-reverse">
                                            {/*========= next =========*/}
                                            {wizardNo < 7 &&
                                            <button type="button" className="btn lg  btn-dark">
                                                <div className="d-flex align-items-center"
                                                     onClick={(e) => gotoWizard(wizardNo + 1)}>
                                                    <div className="title">NEXT</div>
                                                    <div className="icon"><i
                                                        className="typcn typcn-arrow-right"/></div>
                                                </div>
                                            </button>}

                                            {/*======== save ========*/}
                                            {wizardNo === 7 &&
                                            <button type="button" className="btn lg  btn-dark">
                                                {!isSaving &&
                                                <div className="d-flex align-items-center"
                                                     onClick={(e) => saveOffer()}>
                                                    <div className="title">SAVE</div>
                                                    <div className="icon"><i className="typcn typcn-tick"/>
                                                    </div>
                                                </div>
                                                }
                                                {isSaving &&
                                                <div className="d-flex align-items-center">
                                                    <div className="title">Please wait</div>
                                                </div>
                                                }
                                            </button>}

                                            {/*======== back =========*/}
                                            {wizardNo !== 1 &&
                                            <button type="button" className="btn lg   mr-4">
                                                <div className="d-flex align-items-center"
                                                     onClick={(e) => goBackToWizard(wizardNo - 1)}>
                                                    <div className="title">BACK</div>
                                                </div>
                                            </button>
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
};


export default CreateNewOffer;