import React, {Component} from 'react';
import * as authentication from "../../services/authentication";
import {message} from 'antd';
import {connect} from 'react-redux';
import {getAllOffer} from "../../services/offerApi"
import Moment from 'react-moment';
import * as config from "../../config/serverIP";

class Offers extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        //on load get all category with out pagination
        //todo
        this.props.getAllOffer();
    }



    render() {
        const {offerData = [], isLoading} = this.props;
        const row = offerData.map((d, key) =>
                <div className="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 ">
                    <div className="row">
                        <div className="pending-offer-card">
                            <div className="p-offer-header">
                                <div className="title">
                                    {d.label}
                                </div>
                                <div className="header">
                                    {d.header}
                                </div>
                            </div>

                            <div className="p-offer-banner">
                                <img className="bank-logo img-fluid"
                                     alt="Responsive image"
                                     src={config.IMAGE_URL + d.imageId}/>
                            </div>

                            <div className="category-bank-wrapper d-flex">
                                <div className="category-wrp d-flex flex-column text-left">
                                    <div className="title text-left">Category</div>
                                    <div className="category text-left">
                                        {d.category.category}
                                    </div>
                                </div>
                                <div className="category-wrp d-flex flex-column text-left">
                                    <div className="title text-left">Bank</div>
                                    <div className="category text-left">
                                        {d.bank.bankName}
                                    </div>
                                </div>
                            </div>

                            <div className="category-bank-wrapper d-flex mt-2">
                                <div className="category-wrp d-flex flex-column text-left">
                                    <div className="title text-left">start & end date</div>
                                    <div className="category text-left">
                                        {/*{d.offerDateType === 'customize' &&*/}
                                        {/*<>*/}
                                        {/*    {d.customizeDate}*/}
                                        {/*</>*/}
                                        {/*{d.offerDateType !== 'customize' &&*/}
                                        <>
                                            {d.offerStart} {d.offerEnd}
                                        </>
                                        {/*}*/}
                                    </div>
                                </div>
                            </div>

                            <div className="offer-btn-wrapper d-flex  flex-row-reverse">
                                <button type="button" className="btn sm  btn-dark">
                                    <div className="d-flex align-items-center">
                                        <div className="title"> More</div>
                                    </div>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            // <tr key={key}>
            //     <td>
            //         <div className="bank-img-wrp sm">
            //             <img className="bank-logo"
            //                  src={'https://s3.amazonaws.com/sls-imageuploads/media/' + d.imageId}/>
            //         </div>
            //     </td>
            //     <td>{d.offerId}</td>
            //     <td>{d.bank.bankName}</td>
            //     <td>{d.offerStart}</td>
            //     <td>{d.offerEnd}</td>
            //     <td>
            //         <Moment fromNow
            //                 date={d.lastUpdate}/>
            //     </td>
            //     {/*<td>{d.userType}</td>*/}
            //     <td>{d.status}</td>
            // </tr>
        );

        return (
            <div>
                <div
                    className={`loading-wrapper  align-items-center" ${isLoading ? 'active' : 'unload'}`}>
                    Loading...
                </div>

                <div className="row pl-3 pr-3">
                    {row}
                </div>
            </div>
        )
    }
}

Offers.propTypes = {};

const mapStateToProps = state => ({
    offerData: state.offerData.result,
    isLoading: state.offerData.loading,
});

export default connect(mapStateToProps, {getAllOffer})(Offers);