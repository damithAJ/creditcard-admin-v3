import React, {useEffect, useState} from "react";
import {Select} from 'antd';
import Location from '../../../config/json/location';

const LocationAndDescription = props => {

    let tempLocation = null;

    function handleChange(value) {
        props.setLocation(value);
    }

    const {Option} = Select;

    const children = [];
    for (let i = 0; i < Location.length; i++) {
        children.push(
            <Option className="location-tag" key={Location[i]}>
                {Location[i]}
            </Option>);
    }

    return (
        <>
            <div>
                {/*=== valid location =======*/}
                <div className={`form-group  ${props.error === 'location' ? 'is-invalid-multiple' : ''}`}>
                    <label>Offer valid Location <b>*</b></label>
                    <Select
                        mode="multiple"
                        style={{width: '100%'}}
                        placeholder="Please select location"
                        defaultValue={[]}
                        onChange={handleChange}>
                        {children}
                    </Select>,
                </div>

                {/*=== offer description =======*/}
                <div className={`form-group  ${props.error === 'description' ? 'is-invalid' : ''}`}>
                    <label>Description <b>*</b></label>
                    <textarea
                        value={props.description}
                        onChange={e => props.setDescription(e.target.value)}
                        id="description"
                        className="form-control"
                        rows="5"></textarea>
                </div>

            </div>
        </>
    )
};

export default LocationAndDescription;