import React, {useEffect, useState} from "react";
import Moment from "react-moment";
import {Scrollbars} from "react-custom-scrollbars";

const CategoryOffer = props => {
    const categoryDetails = props.categoryData.map((d, key) =>
        <div id={d.clientId}
             onClick={e => props.SetCategoryId(d.categoryId)}
             className={`client-selection-row d-flex justify-content-between ${d.categoryId === props.categoryId ? 'active' : ''}`}>
            <div className="left-wrp d-flex">
                <div className="client-name d-flex-center">
                    {d.category}
                </div>
            </div>
            <div className="right-wrp d-flex-center flex-row-reverse">
                <div className="select-item "/>
            </div>

        </div>
    );
    return (
        <>
            <div
                className={`loading-wrapper  align-items-center" ${props.isCategoryLoading ? 'active' : 'unload'}`}>
                Loading...
            </div>
            {categoryDetails}
        </>
    )
};

export default CategoryOffer;