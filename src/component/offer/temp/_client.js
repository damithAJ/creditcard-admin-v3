import React, {useEffect, useState} from "react";
import Moment from "react-moment";
import {Scrollbars} from "react-custom-scrollbars";
import Autosuggest from 'react-autosuggest';
import * as config from "../../../config/serverIP";

const ClientDetails = props => {
    const clientData = props.clientData.map((d, key) =>
        <div id={d.clientId}
             onClick={e => props.SetClientId(d.clientId)}
             className={`client-selection-row d-flex justify-content-between ${d.clientId === props.clientId ? 'active' : ''}`}>
            <div className="left-wrp d-flex">
                <div className="client-img d-flex-center ">
                    <img className="bank-logo"
                         src={config.IMAGE_URL + d.imageId}/>
                </div>
                <div className="client-name d-flex-center">
                    {d.clientName}
                </div>
            </div>
            <div className="right-wrp d-flex-center flex-row-reverse">
                <div className="select-item  "/>
            </div>
        </div>
    );

    return (
        <>
            <div
                className={`loading-wrapper  align-items-center" ${props.isClientLoading ? 'active' : 'unload'}`}>
                Loading...
            </div>
            {clientData}
        </>

    )
};

export default ClientDetails;