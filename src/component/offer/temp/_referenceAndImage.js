import React, {useEffect, useState} from "react";
import Moment from "react-moment";
import {Scrollbars} from "react-custom-scrollbars";
import FileBase64 from "react-filebase64";


const ReferenceAndImage = props => {
    // Callback~
    const getFiles = (files) => {
        props.setFiles(files);
        props.setBase64(files.base64);
    };

    return (
        <>
            {/*=== reference =======*/}
            <div className={`form-group  ${props.error === 'reference' ? 'is-invalid' : ''}`}>
                <label>Offer Reference URL <b>*</b></label>
                <input id="label"
                       name="label"
                       value={props.reference}
                       onChange={e => props.setReference(e.target.value)}
                       className="form-control lg"
                       placeholder="Ex : https://www.sampath.lk/en/sampath-credit-card-offers/amex-fuel-31-12-2020"/>
            </div>

            <div
                className={`form-group file-upload ${props.error === 'upload' ? 'is-invalid' : ''}`}>
                <label>Offer Banner image (900x277)  <b>*</b></label>
                <br/>
                <div className="d-flex">
                    <div className="uploader float-left">
                        <FileBase64
                            multiple={false}
                            onDone={getFiles.bind(this)}/>
                    </div>
                </div>
            </div>
        </>

    )
};

export default ReferenceAndImage;