import React, {useEffect, useState} from "react";
import Moment from "react-moment";
import {Scrollbars} from "react-custom-scrollbars";
import {DatePicker, Select, Form, Input} from 'antd';

const OfferDetails = props => {
    const {RangePicker} = DatePicker;
    console.log(props);

    function onChangeStartEndDate(date, dateString) {
        console.log(date);
        console.log(dateString);
        if (dateString && dateString.length !== 0) {
            props.SetStartDate(dateString[0]);
            props.SetEndDate(dateString[1]);
        }
    }


    function onChangeDateType(value) {
        props.setOfferDateType(value);
    }

    //card type
    function onChangeCardType(value) {
        props.setCardType(value);
    }

    //off type
    function onChangeOffType(value) {
        props.setOffType(value);
    }

    const {Option} = Select;

    return (
        <>
            <div>
                {/*=== offer =======*/}
                <div className={`form-group  ${props.error === 'label' ? 'is-invalid' : ''}`}>
                    <label>Offer title <b>*</b></label>
                    <input id="label"
                           name="label"
                           value={props.label}
                           onChange={e => props.setLabel(e.target.value)}
                           className="form-control lg"
                           placeholder="Ex : Hotel Name, Restaurant Name"/>
                </div>

                {/*=== offer header =======*/}
                <div className={`form-group  ${props.error === 'header' ? 'is-invalid' : ''}`}>
                    <label>Offer header <b>*</b></label>
                    <input id="header"
                           type="text"
                           name="header"
                           value={props.header}
                           onChange={e => props.SetHeader(e.target.value)}
                           className="form-control lg"
                           placeholder="Ex : ABC Hotel - Kandy"/>
                </div>

                {/*=== offer date type and offer date =======*/}
                <div className={`form-group`}>
                    <div className="row">
                        <div className="col-5 pr-0">
                            <div>
                                <label>Offer Date Type <b>*</b></label>
                                <br/>
                                <Select
                                    value={props.offerDateType}
                                    style={{width: '100%'}}
                                    onChange={onChangeDateType}>
                                    <Option value="to">to</Option>
                                    <Option value="until">Until</Option>
                                    <Option value="customize">Customize </Option>
                                </Select>
                            </div>

                        </div>
                        <div className="col-7">
                            {/*==== to ===== */}
                            {props.offerDateType === "to" &&
                            <div  className={`form-group  ${props.error === 'dateRange' ? 'is-invalid-date' : ''}`}>
                                <label>Offer Start & End date <b>*</b></label>
                                <br/>
                                <RangePicker size="large"
                                             separator="-"
                                             onChange={onChangeStartEndDate}
                                             format="YYYY-MM-DD"/>
                            </div>
                            }
                            {/*==== until ===== */}
                            {props.offerDateType === "until" &&
                            <div className={`form-group  ${props.error === 'dateRange' ? 'is-invalid-date' : ''}`}>
                                <label>Offer End date (Until) <b>*</b></label>
                                <br/>
                                <RangePicker size="large"
                                             onChange={onChangeStartEndDate}
                                             format="YYYY-MM-DD"/>
                            </div>
                            }

                            {/*==== customize ===== */}
                            {props.offerDateType === "customize" &&
                            <div className={`form-group  ${props.error === 'customize' ? 'is-invalid' : ''}`}>
                                <label>Customize Date <b>*</b></label>
                                <br/>
                                <input id="customize"
                                       name="customize"
                                       value={props.customizeDate}
                                       onChange={e => props.SetCustomizeDate(e.target.value)}
                                       className="form-control lg"
                                       placeholder="Ex : 1st - 5th January 2020"/>
                            </div>
                            }

                        </div>
                    </div>
                </div>

                {/*=== offer card type / rate / offer rate =======*/}
                <div className={`form-group`}>
                    <div className="row">
                        {/*card type*/}
                        <div className="col-5 pr-0">
                            <div>
                                <label>Card Type <b>*</b></label>
                                <br/>
                                <Select
                                    value={props.cardType}
                                    style={{width: '100%'}}
                                    placeholder="ex : Credit"
                                    onChange={onChangeCardType}>
                                    <Option value="credit">Credit Cards</Option>
                                    <Option value="debit">Debit Cards</Option>
                                    <Option value="visa">Visa Cards</Option>
                                    <Option value="credit&debit">Credit & Debit Cards</Option>
                                </Select>
                            </div>

                        </div>
                        {/*Off Type*/}
                        <div className="col-3 pr-0">
                            <div>
                                <label>Off Type <b>*</b></label>
                                <br/>
                                <Select
                                    value={props.offType}
                                    style={{width: '100%'}}
                                    onChange={onChangeOffType}>
                                    <Option value="upto">Up to</Option>
                                    <Option value="off">Off</Option>
                                </Select>
                            </div>

                        </div>
                        {/*Offer Rate */}
                        <div className="col-4">
                            <div className={`form-group  ${props.error === 'offerRate' ? 'is-invalid' : ''}`}>
                                <label>Offer Rate % <b>*</b></label>
                                <br/>
                                <input id="offerRate"
                                       name="offerRate"
                                       type="number"
                                       value={props.offerRate}
                                       onChange={e => props.setOfferRate(e.target.value)}
                                       className="form-control lg"
                                       placeholder="Ex : 20%"/>
                            </div>

                        </div>
                    </div>
                </div>

                {/*=== contact no =======*/}
                <div className={`form-group`}>
                    <label>Contact No <b>*</b> </label>



                    <input id="label"
                           name="label"
                           value={props.contactNo}
                           onChange={e => props.setContactNo(e.target.value)}
                           className="form-control lg"
                           placeholder="Ex : +94 00 000 000"/>
                </div>

            </div>
        </>
    )
};

export default OfferDetails;