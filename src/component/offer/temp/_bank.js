import React, {useEffect, useState} from "react";
import * as config from "../../../config/serverIP";
import Moment from "react-moment";
import {Scrollbars} from "react-custom-scrollbars";

const bankOffer = props => {
    const bankDetails = props.bankData.map((d, key) =>
        <div id={d.bankId}
             onClick={e => props.SetBankId(d.bankId)}
             className={`bank-card-wrapper d-flex flex-column align-items-center"
              ${d.bankId === props.bankId ? 'active' : ''}`}
        >

            <div className="select-item"></div>

            <div className="logo">
                <img className="bank-logo"
                     src={config.IMAGE_URL + d.imageId}/>
            </div>
            <div className="bank-name">
                {d.bankName}
            </div>
        </div>
    );
    return (

        <>
            <div
                className={`loading-wrapper  align-items-center" ${props.isBankLoading ? 'active' : 'unload'}`}>
                Loading...
            </div>
            <div className="d-flex flex-wrap">
                {bankDetails}
            </div>
        </>

    )
};

export default bankOffer;