import React, {useEffect, useState} from "react";
import {List, Avatar} from 'antd';

const TermsAndConditions = props => {


    function addToList() {
        props.setTermsConditions(props.termsConditions.concat(props.terms));
        props.setTerms('');
    }

    const deleteItem = null;
    const handleRemoveItem = term => {
        //deleteItem = term;
        const temp = [...props.termsConditions];
        temp.splice(term, 1);
        props.setTermsConditions(temp);
    };


    return (

        <>
            <div>
                {/*=== valid location =======*/}
                <div className={`form-group d-flex`}>
                    <div className={`text-box flex-grow-1  ${props.error === 'terms' ? 'is-invalid' : ''}`}>
                        <label>Terms And Conditions <b>*</b></label>
                        <input
                            type="text"
                            style={{width: '100%'}}
                            id="termsAndConditions"
                            className="form-control lg"
                            name="termsAndConditions"
                            value={props.terms}
                            placeholder="Please add terms and Conditions"
                            onChange={e => props.setTerms(e.target.value)}>
                        </input>
                    </div>

                    <div className="button-wrp">
                        <button className="btn btn-dark btn-add"
                                onClick={addToList}>
                            Add
                        </button>
                    </div>
                </div>
                <div className="list-term-wrp d-flex flex-column">
                    {props.termsConditions.map((term, index) => (
                        <div key={index} className="term-item">
                            {term}
                            <div className="delete-icon  d-flex d-flex-center"
                                 onClick={e => handleRemoveItem(term)}>
                                <i className="typcn typcn-trash"/>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        </>
    )
};

export default TermsAndConditions;